var wsproto = (location.protocol === 'https:') ? 'wss:' : 'ws:';
var conn = wsproto + '//' + window.location.host + '/signal.ump';
UmVideo.setup({
    //signalserver: "wss://localhost:4430/signal"  // or
    //signalserver: "ws://localhost:8088/signal"
    signalserver: conn,
    getusermedianow: true,
    role: "camera"
});

var list_arr = [];
var link = window.location.href;
var new_link = link.replace(/camera_list/g, "camera_view");
UmVideo.on("ready", ()=>{
    UmVideo.listRooms();
    UmVideo.on("roomlist", (rooms)=>{
        console.log("房间列表", rooms);
        list_arr = rooms.rooms;
        for (let i = 0; i < list_arr.length; i++) {
            var father = document.getElementById("list_ul");
            var cre_a = document.createElement("a");
            var cre_li = document.createElement("li");
            var cre_img = document.createElement("img");
            var cre_div = document.createElement("div");
            cre_a.href = new_link + "?h=" + list_arr[i].name + "&cid=" + list_arr[i].cid;
            cre_li.className = "list_li";
            cre_img.src = "/img/CCD.png";
            cre_div.innerHTML = list_arr[i].info;
            father.appendChild(cre_li);
            cre_li.appendChild(cre_a);
            cre_a.appendChild(cre_img);
            cre_a.appendChild(cre_div);
        }
    })
})
